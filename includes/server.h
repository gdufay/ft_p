/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:21:13 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 13:40:30 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "ft_p.h"
# include <dirent.h>
# include <sys/wait.h>
# include <limits.h>

# define PADDING 10

/*
** time is a bonus to have more infos in log
*/

# include <time.h>

# define ROOT_DIR "anonymous"
# define ROOT_DIR_LEN sizeof(ROOT_DIR) / sizeof(*ROOT_DIR)

typedef struct	s_root
{
	char	root[PATH_MAX];
	char	*name;
	size_t	len;
}				t_root;

extern t_root	g_root;

/*
** Code error
*/

enum			e_replies
{
	OPEN_CONN = 1,
	SUCCESS,
	SUPERFLUOUS_CMD,
	USER_CONNECT,
	CONN_CLOSED,
	USER_LOGGED,
	DIR_CREATED,
	NEED_PW,
	NO_DATA_CONN,
	LOCAL_ERROR,
	SYNTAX_ERROR,
	CMD_NOT_IMPLM,
	BAD_CMD_SEQ,
	USER_NOT_LOGGED,
	FILE_UNVL,
	PWD_CMD,
	CLOSE_CTRL_CONN,
};

# define REQ_SIZE 2048

/*
** port command define
*/

# define PORT_ARGLEN 6
# define PORT_HOSTLEN 3

/*
** ls command define
*/

# define LS_PATH "/bin/ls"
# define LS_OPT "-l"
# define LS_ERR 42

/*
** define for password system
*/

# define SALT "IAmTheSalt"
# define PW_FILE "pw_file"

/*
** struct to saved socket information
*/

struct			s_sock
{
	int					sock;
	struct sockaddr_in	sin;
};

/*
** enum for s_conn.auth status
*/

enum			e_auth
{
	NOT_AUTH,
	AUTH,
	IN_PROG,
};

struct			s_auth
{
	char		*name;
	enum e_auth	auth;
};

/*
** t_conn handle connection for one client
*/

typedef struct	s_conn
{
	int				user;
	struct s_auth	*auth;
	char			*name;
	char			*cmd;
	char			**params;
	struct s_sock	ftp;
	struct s_sock	data;
}				t_conn;

typedef struct	s_cmd
{
	char	*cmd;
	void	(*ft)(char**, struct s_conn*);
}				t_cmd;

int				init_filesystem(void);
int				init_g_root(void);
void			parse_req(t_conn conn);
void			replies(t_conn conn, enum e_replies reply);
void			custom_replies(t_conn conn, enum e_replies reply, char *s);
void			server(int sock);

/*
 ** SOCKET
*/

int				create_socket(struct sockaddr_in sin,
		int (*ft)(int, struct sockaddr_in*));
int				connect_socket(int sock, struct sockaddr_in *sin);
int				bind_socket(int sock, struct sockaddr_in *sin);

/*
** COMMANDS
*/

void			ft_ls(char **params, t_conn *conn);
void			ft_cd(char **params, t_conn *conn);
void			ft_pwd(char **params, t_conn *conn);
void			ft_mkdir(char **params, t_conn *conn);
void			ft_quit(char **params, t_conn *conn);
void			ft_port(char **params, t_conn *conn);
void			ft_retr(char **params, t_conn *conn);
void			ft_stor(char **params, t_conn *conn);
void			ft_user(char **params, t_conn *conn);
void			ft_pass(char **params, t_conn *conn);

/*
** UTILS
*/

void			ft_log(int user, char *cmd, char *state);
char			*hosttoserver(char *buf);
char			*servertohost(char *path, char *buf);
char			*canonise_path(char *path, char *buf);
char			*getentry_pwfile(char *name);
int				create_or_chdir(char *name);

#endif
