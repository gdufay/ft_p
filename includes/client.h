/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:19:04 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 10:35:55 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_H
# define CLIENT_H

# include "ft_p.h"
# include <sys/types.h>

# define BUF_SIZE 1024
# define PADDING 10

/*
** define client commands
*/

# define LS "ls"
# define CD "cd"
# define GET "get"
# define PUT "put"
# define MKDIR "mkdir"
# define LLS "lls"
# define LCD "lcd"
# define LPWD "lpwd"
# define UNKNW_CMD "Command unknown\n"

/*
** ls command define
*/

# define LS_PATH "/bin/ls"
# define LS_ARGC 128

/*
** prompt define
*/

# define PROMPT "$> "
# define END_PROMPT "> "
# define END_PROMPT_LEN sizeof(END_PROMPT)

/*
** error define
*/

enum			e_bool
{
	SUCCESS = 0,
	ERROR,
};

# define BAD_CMD "Invalid command.\n"

typedef struct	s_associate
{
	char	*client;
	int		(*ft)(int, char**);
}				t_associate;

int				prompt(int sock);
void			handle_replies(char *rep);
int				write_cmd(int sock, char **cmd);
int				listening_socket(int sock);
int				accept_conn(int sock);

/*
** Commands
*/

int				ft_ls(int sock, char **cmd);
int				ft_cd(int sock, char **cmd);
int				ft_quit(int sock, char **cmd);
int				ft_pwd(int sock, char **cmd);
int				ft_get(int sock, char **cmd);
int				ft_put(int sock, char **cmd);
int				ft_port(int sock, char **cmd);
int				ft_user(int sock, char **cmd);
int				ft_mkdir(int sock, char **cmd);
int				ft_lls(int sock, char **cmd);
int				ft_lcd(int sock, char **cmd);
int				ft_lpwd(int sock, char **cmd);

#endif
