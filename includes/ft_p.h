/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:33:40 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/18 11:12:23 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_P_H
# define FT_P_H

# include "libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <sys/socket.h>
# include <netdb.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <signal.h>
# include <sys/stat.h>
# include <sys/mman.h>

# define PORT_MAXLEN (1 << 16) - 1

/*
** Commands
*/

# define LIST "list"
# define NLST "nlst"
# define CWD "cwd"
# define PWD "pwd"
# define MKD "mkd"
# define QUIT "quit"
# define PORT "port"
# define RETR "retr"
# define STOR "stor"
# define USER "user"
# define PASS "pass"

/*
** Host and network EOF
*/

# define H_CRLF "\n"
# define S_CRLF "\r\n"

#endif
