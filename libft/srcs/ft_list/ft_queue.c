/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queue.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 12:11:55 by gdufay            #+#    #+#             */
/*   Updated: 2018/10/18 13:20:19 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_queue	*ft_queuenew(t_list *front, t_list *rear)
{
	t_queue *tmp;

	if (!(tmp = (t_queue*)malloc(sizeof(*tmp))))
		return (NULL);
	tmp->front = front;
	tmp->rear = rear;
	return (tmp);
}

int		ft_enqueue(t_queue *queue, t_list *elem)
{
	if (!queue || !elem)
		return (0);
	if (!queue->front)
		queue->front = elem;
	if (queue->rear)
		queue->rear->next = elem;
	queue->rear = elem;
	return (1);
}

t_list	*ft_dequeue(t_queue *queue)
{
	t_list	*tmp;

	if (!queue || !queue->front)
		return (NULL);
	tmp = queue->front;
	queue->front = queue->front->next;
	if (!queue->front)
		queue->rear = NULL;
	return (tmp);
}
