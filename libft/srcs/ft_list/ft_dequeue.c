/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dequeue.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 13:51:49 by gdufay            #+#    #+#             */
/*   Updated: 2018/10/18 14:40:59 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_dq_insertfront(t_queue *dequeue, t_list *elem)
{
	if (!dequeue || !elem)
		return (0);
	if (!dequeue->front)
		ft_enqueue(dequeue, elem);
	else
		ft_stackpush(&dequeue->front, elem);
	return (1);
}

t_list	*ft_dq_deletefront(t_queue *dequeue)
{
	t_list	*tmp;

	if (!dequeue)
		return (NULL);
	tmp = ft_stackpop(&dequeue->front);
	if (!dequeue->front)
		dequeue->rear = NULL;
	return (tmp);
}

int		ft_dq_insertrear(t_queue *dequeue, t_list *elem)
{
	return (ft_enqueue(dequeue, elem));
}

/*
** t_list	*ft_dq_deleterear()
** {
** }
*/
