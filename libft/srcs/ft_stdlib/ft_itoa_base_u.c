/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_u.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/11 17:17:50 by gdufay            #+#    #+#             */
/*   Updated: 2019/06/12 10:20:01 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa_base_u(size_t n, int base, int up)
{
	char	*tab;
	int		i;
	size_t	nb;

	i = 1;
	nb = n;
	while ((nb /= base))
		i++;
	if (!(tab = (char*)malloc(sizeof(*tab) * (i + 1))))
		return (NULL);
	tab[i] = 0;
	while (i--)
	{
		nb = (up ? 'A' : 'a');
		tab[i] = (n % base < 10 ? n % base + 48 : n % base + nb - 10);
		n /= base;
	}
	return (tab);
}
