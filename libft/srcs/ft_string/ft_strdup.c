/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 16:08:00 by gdufay            #+#    #+#             */
/*   Updated: 2019/06/12 11:14:20 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*dup;
	char	*ret;

	if (!s1 || !*s1)
		return (NULL);
	if (!(dup = (char*)malloc(sizeof(*dup) * (ft_strlen(s1) + 1))))
		return (NULL);
	ret = dup;
	while (*s1)
		*dup++ = *s1++;
	*dup = 0;
	return (ret);
}

char	*ft_strndup(const char *s1, size_t n)
{
	char	*dup;
	char	*ret;
	size_t	i;

	if (!s1 || !n)
		return (NULL);
	i = ft_strnlen(s1, n);
	if (i < n)
		n = i;
	if (!(dup = (char*)malloc(sizeof(*dup) * (n + 1))))
		return (NULL);
	ret = dup;
	while (n-- && *s1)
		*dup++ = *s1++;
	*dup = 0;
	return (ret);
}
