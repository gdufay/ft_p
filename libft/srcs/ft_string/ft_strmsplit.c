/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmsplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 10:50:28 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/16 13:41:21 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** fill_buffer set the corresponding bit in buf
*/

static void		fill_buffer(__uint128_t *buf, char *p)
{
	while (*p)
	{
		if (ft_isascii(*p))
			*buf = (*buf ^ ((__uint128_t)1 << *p));
		p++;
	}
}

/*
** get the size to malloc **ret
*/

static size_t	count_split(char *s, __uint128_t buf)
{
	size_t	ret;
	int		isword;

	ret = 0;
	isword = 0;
	while (*s)
	{
		if (!ft_isascii(*s))
			return (0);
		if (!isword && !(buf & ((__uint128_t)1 << *s)))
		{
			ret++;
			isword = 1;
		}
		if (isword && (buf & ((__uint128_t)1 << *s)))
			isword = 0;
		s++;
	}
	return (ret);
}

static void		tabdup(char **ret, char *s, __uint128_t buf)
{
	size_t	i;

	if (!*s)
	{
		*ret = NULL;
		return ;
	}
	while ((buf & ((__uint128_t)1 << *s)))
		s++;
	i = 0;
	while (*(s + i) && !(buf & ((__uint128_t)1 << *(s + i))))
		i++;
	if ((*ret = ft_strndup(s, i)) == NULL)
		return ;
	tabdup(ret + 1, s + i, buf);
}

char			**ft_strmsplit(char *s, char *p)
{
	__uint128_t	buf;
	char		**ret;
	size_t		len;

	buf = 0;
	fill_buffer(&buf, p);
	if ((len = count_split(s, buf)) == 0
			|| (ret = malloc(sizeof(*ret) * ++len)) == NULL)
		return (NULL);
	tabdup(ret, s, buf);
	return (ret);
}
