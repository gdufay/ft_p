/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 10:24:21 by gdufay            #+#    #+#             */
/*   Updated: 2019/07/24 10:37:59 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			get_next_line(const int fd, char **line)
{
	char		buf[BUF_SIZE];
	static char	tmp[4096][LINE_MAX];
	int			ret;
	char		*find;

	if (!line)
		return (-1);
	while ((ret = read(fd, buf, BUF_SIZE)) > 0)
	{
		buf[ret] = 0;
		ft_strcat(tmp[fd], buf);
		if (ft_strchr(buf, '\n'))
			break ;
	}
	find = ft_strchr(tmp[fd], '\n');
	if (find)
	{
		*line = ft_strndup(tmp[fd], find - tmp[fd]);
		ft_strcpy(tmp[fd], find + 1);
	}
	else
		*line = NULL;
	if (ret == -1)
		return (-1);
	return (!!*tmp[fd]);
}
