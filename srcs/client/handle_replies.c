/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_replies.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/25 11:38:13 by gdufay            #+#    #+#             */
/*   Updated: 2019/07/25 12:11:07 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static int	is_single_line(char *rep)
{
	return (rep[3] == ' ');
}

static void	print_multi_line(char *rep)
{
	char	*nl;
	char	*tmp;
	char	buf[PATH_MAX + 1];

	nl = ft_strchr(rep, '\n') + 1;
	while (*nl)
	{
		tmp = ft_strchr(nl, '\n') + 1;
		if (!*tmp)
			break ;
		ft_strncpy(buf, nl, tmp - nl);
		ft_putstr(buf);
		nl = tmp;
	}
}

void		handle_replies(char *rep)
{
	if (is_single_line(rep))
		ft_putstr(rep + 4);
	else
		print_multi_line(rep);
	printf("Code: %.3s --> %s\n", rep, *rep == '2' ? "Success" : "Error");
}
