/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:20:16 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 15:09:02 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	usage(char *name)
{
	printf("%s server port\n", name);
	exit(EXIT_FAILURE);
}

static char	*host_to_ip(char *addr)
{
	struct hostent	*host;
	struct in_addr	**list;

	if ((host = gethostbyname(addr)) == NULL
			|| (list = (struct in_addr**)host->h_addr_list) == NULL
			|| *list == NULL)
	{
		return (addr);
	}
	return (inet_ntoa(**list));
}

int			create_client(char *addr, int port)
{
	int					sock;
	struct protoent		*proto;
	struct sockaddr_in	sin;
	struct timeval		tv;

	if ((proto = getprotobyname("tcp")) == NULL)
		return (-1);
	sock = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	tv.tv_sec = 2;
	tv.tv_usec = 2000;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = inet_addr(host_to_ip(addr));
	if (connect(sock, (const struct sockaddr*)&sin, sizeof(sin)) == -1)
	{
		close(sock);
		ft_putstr_fd("Error connecting\n", 2);
		exit(EXIT_FAILURE);
	}
	return (sock);
}

int			main(int ac, char **av)
{
	int		port;
	int		sock;
	int		ret;

	if (ac != 3)
		usage(av[0]);
	port = ft_atoi(av[2]);
	sock = create_client(av[1], port);
	ret = prompt(sock);
	close(sock);
	return (ret);
}
