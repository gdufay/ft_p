/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 12:31:54 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 12:28:15 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	recv_file(int sock, int fd)
{
	char	buf[BUF_SIZE + 1];
	ssize_t	ret;
	ssize_t	wr;
	ssize_t	sum;

	sum = 0;
	while ((ret = read(sock, buf, BUF_SIZE)) > 0)
	{
		buf[ret] = 0;
		wr = write(fd, buf, ret);
		if (wr == -1 || wr != ret)
			break ;
		sum += wr;
		printf("\33[2K\rReceiving in progress: %ld octets", sum);
		fflush(stdout);
	}
	ft_putchar('\n');
}

static void	user_pi(int sock, char *file)
{
	char	buf[PATH_MAX + PADDING];

	ft_strcpy(buf, "retr ");
	ft_strncat(buf, file, PATH_MAX);
	write(sock, buf, ft_strlen(buf));
}

static int	server_res(int sock)
{
	char	*s;
	int		ret;

	get_next_line(sock, &s);
	if (s == NULL)
		return (ERROR);
	printf("%s\n", s);
	ret = ft_atoi(s) > 150 ? ERROR : SUCCESS;
	free(s);
	return (ret);
}

static int	open_file(char *file)
{
	char	*last;
	int		fd;

	if ((last = ft_strrchr(file, '/')) == NULL)
		last = file;
	else
		last += 1;
	if ((fd = open(last, O_RDWR | O_CREAT | O_TRUNC, 0644)) < 0)
	{
		printf("Can't open file: %s\n", last);
		return (ERROR);
	}
	return (fd);
}

int			ft_get(int sock, char **cmd)
{
	int		list;
	int		acc;
	int		fd;
	int		ret;

	if (!*cmd)
		return (!!printf(BAD_CMD));
	else if ((fd = open_file(*cmd)) == ERROR)
		return (ERROR);
	else if ((list = listening_socket(sock)) == ERROR)
	{
		close(fd);
		printf("Can't listen socket\n");
		return (ERROR);
	}
	user_pi(sock, *cmd);
	if ((ret = server_res(sock)) != ERROR)
	{
		acc = accept_conn(list);
		recv_file(acc, fd);
		close(acc);
	}
	close(list);
	close(fd);
	return (ret == ERROR ? ERROR : SUCCESS);
}
