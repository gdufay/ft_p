/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:16:15 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 14:08:43 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int		write_cmd(int sock, char **cmd)
{
	size_t				i;
	static t_associate	buf[] = {
		{LS, ft_ls}, {CD, ft_cd}, {QUIT, ft_quit}, {PWD, ft_pwd},
		{GET, ft_get}, {PUT, ft_put}, {USER, ft_user}, {MKDIR, ft_mkdir},
		{LLS, ft_lls}, {LCD, ft_lcd}, {LPWD, ft_lpwd},
	};

	i = -1;
	while (++i < sizeof(buf) / sizeof(*buf))
		if (!ft_strcmp(buf[i].client, *cmd))
			return (buf[i].ft(sock, cmd + 1));
	printf(UNKNW_CMD);
	return (ERROR);
}
