/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lcd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 15:34:55 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 10:15:27 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int		ft_lcd(int sock, char **params)
{
	(void)sock;
	if (chdir(*params) < 0)
		printf("Problems cd to %s\n", *params);
	else
		ft_lpwd(-1, NULL);
	return (ERROR);
}
