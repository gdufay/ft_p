/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mkdir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 12:37:31 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/17 10:16:44 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int		ft_mkdir(int sock, char **cmd)
{
	char	buf[PATH_MAX + PADDING];

	if (!*cmd)
	{
		printf(BAD_CMD);
		return (ERROR);
	}
	ft_strcpy(buf, "mkd ");
	ft_strncat(buf, *cmd, PATH_MAX);
	write(sock, buf, ft_strlen(buf));
	return (SUCCESS);
}
