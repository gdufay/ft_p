/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_conn.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 14:32:59 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/17 14:34:36 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int		listening_socket(int sock)
{
	int					ret;
	struct sockaddr_in	sin;
	struct protoent		*proto;
	unsigned int		len;
	int					opt;

	len = sizeof(sin);
	if (getsockname(sock, (struct sockaddr*)&sin, &len) < 0
			|| (proto = getprotobyname("tcp")) == NULL)
		return (ERROR);
	ret = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	opt = 1;
	setsockopt(ret, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt));
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(ret, (struct sockaddr*)&sin, sizeof(sin)) < 0)
	{
		close(ret);
		return (ERROR);
	}
	listen(ret, 5);
	return (ret);
}

int		accept_conn(int sock)
{
	struct sockaddr_in	csin;
	unsigned int		cslen;
	int					cs;

	cslen = sizeof(csin);
	cs = accept(sock, (struct sockaddr*)&csin, &cslen);
	close(sock);
	return (cs);
}
