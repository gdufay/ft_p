/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 09:22:17 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 14:02:51 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static int	read_loop(int sock)
{
	char	*s;

	get_next_line(sock, &s);
	if (s == NULL)
	{
		printf("Cannot listen socket, closing...\n");
		return (-1);
	}
	ft_putendl(s);
	ft_strdel(&s);
	return (0);
}

static void	beautiful_prompt(char *buf)
{
	if (getcwd(buf, PATH_MAX) == NULL)
		ft_strcpy(buf, PROMPT);
	else
		ft_strcat(buf, END_PROMPT);
}

int			prompt(int sock)
{
	char	*s;
	char	buf[PATH_MAX + END_PROMPT_LEN];
	int		cmp;
	char	**split;
	int		ret;

	read_loop(sock);
	ret = 0;
	while (1)
	{
		beautiful_prompt(buf);
		ft_putstr(buf);
		if (get_next_line(STDIN_FILENO, &s) == -1)
			return (-1);
		if (s == NULL || (split = ft_strmsplit(s, " \n\t\v\r\f")) == NULL)
			continue ;
		if (!write_cmd(sock, split))
			ret = read_loop(sock);
		cmp = ft_strcmp(s, "quit");
		ft_strdel(&s);
		ft_free_tab(&split);
		if (cmp == 0 || ret)
			break ;
	}
	return (0);
}
