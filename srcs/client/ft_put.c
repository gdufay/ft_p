/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 12:33:27 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 11:37:42 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	print_loadbar(off_t sum, off_t st_size)
{
	int			progress;
	char		buf[26];
	int			i;
	static int	count = 0;

	if (++count < 50 && sum != st_size)
		return ;
	else
		count = 0;
	progress = (float)sum / (float)st_size * 100;
	i = -1;
	while (++i < progress / 4)
		buf[i] = '=';
	buf[i++] = '>';
	buf[i] = 0;
	printf("\33[2K\rSending in progress: [%-26s] %d%%", buf, progress);
	fflush(stdout);
}

static int	send_file(int sock, int fd)
{
	struct stat	buf;
	char		r[BUF_SIZE + 1];
	ssize_t		ret;
	off_t		sum;

	if (fstat(fd, &buf) < 0 || buf.st_size == 0)
		return (ERROR);
	sum = 0;
	signal(SIGPIPE, SIG_IGN);
	while ((ret = read(fd, r, BUF_SIZE)) > 0)
	{
		r[ret] = 0;
		if (write(sock, r, ret) != ret)
			break ;
		sum += ret;
		print_loadbar(sum, buf.st_size);
	}
	ft_putchar('\n');
	if (sum == buf.st_size)
		printf("Send !\n");
	else
		printf("Something went wrong uploading the file\n");
	signal(SIGPIPE, SIG_DFL);
	return (SUCCESS);
}

static void	user_pi(int sock, char *file)
{
	char	buf[PATH_MAX + PADDING];

	ft_strcpy(buf, "stor ");
	ft_strncat(buf, file, PATH_MAX);
	write(sock, buf, ft_strlen(buf));
}

static int	server_res(int sock)
{
	char	*s;
	int		ret;

	get_next_line(sock, &s);
	if (s == NULL)
		return (ERROR);
	printf("%s\n", s);
	ret = ft_atoi(s) > 150 ? ERROR : SUCCESS;
	free(s);
	return (ret);
}

int			ft_put(int sock, char **cmd)
{
	int		list;
	int		acc;
	int		fd;

	if (!*cmd || (fd = open(*cmd, O_RDONLY)) < 0)
		return (!!printf(BAD_CMD));
	if ((list = listening_socket(sock)) == ERROR)
	{
		close(fd);
		ft_putstr("Something went wrong\n");
		return (ERROR);
	}
	user_pi(sock, *cmd);
	if (server_res(sock) == ERROR)
	{
		close(list);
		close(fd);
		return (ERROR);
	}
	acc = accept_conn(list);
	if (send_file(acc, fd) == ERROR)
		ft_putstr("Something went wrong\n");
	close(acc);
	close(fd);
	return (SUCCESS);
}
