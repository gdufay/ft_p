/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_user.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 12:36:50 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 14:02:05 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static int	ask_pw(int sock)
{
	char	buf[BUF_SIZE];
	char	join[BUF_SIZE + PADDING];
	ssize_t	ret;

	ft_putstr("Password: ");
	if ((ret = read(STDIN_FILENO, buf, BUF_SIZE - 1)) >= 0)
	{
		buf[ret] = 0;
		ft_strcpy(join, "pass ");
		ft_strcat(join, buf);
		write(sock, join, ft_strlen(join));
		return (SUCCESS);
	}
	return (ERROR);
}

int			ft_user(int sock, char **cmd)
{
	char	buf[PATH_MAX + PADDING];
	char	*s;
	int		ret;
	int		i;

	if (!*cmd)
	{
		printf(BAD_CMD);
		return (ERROR);
	}
	ft_strcpy(buf, "user ");
	ft_strncat(buf, *cmd, PATH_MAX);
	write(sock, buf, ft_strlen(buf));
	get_next_line(sock, &s);
	ret = ERROR;
	i = 0;
	if (s)
		i = ft_atoi(s);
	if (i == 331)
		ret = ask_pw(sock);
	else
		printf("%s\n", s);
	ft_strdel(&s);
	return (i == 421 ? SUCCESS : ret);
}
