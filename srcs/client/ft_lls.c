/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lls.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 15:33:37 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 10:44:44 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	fill_argv(char *argv[LS_ARGC], char **params)
{
	size_t	i;

	i = 0;
	argv[0] = LS_PATH;
	while (++i < LS_ARGC && *params)
	{
		argv[i] = *params;
		params++;
	}
	argv[i] = NULL;
}

int			ft_lls(int sock, char **params)
{
	pid_t	pid;
	int		stat;
	char	*argv[LS_ARGC];

	(void)sock;
	if ((pid = fork()) < 0)
	{
		printf("Problems forking\n");
		return (ERROR);
	}
	else if (pid > 0)
		wait4(pid, &stat, 0, 0);
	else
	{
		fill_argv(argv, params);
		execv(LS_PATH, argv);
		exit(EXIT_SUCCESS);
	}
	return (ERROR);
}
