/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 11:46:29 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 11:07:52 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	write_params(int sock, char *cmd)
{
	char	buf[PATH_MAX + PADDING];

	ft_strcpy(buf, LIST);
	if (cmd)
	{
		buf[sizeof(LIST) - 1] = ' ';
		ft_strncpy(buf + sizeof(LIST), cmd, PATH_MAX);
	}
	write(sock, buf, ft_strlen(buf));
}

int			ft_ls(int sock, char **cmd)
{
	int		list;
	int		acc;
	char	buf[BUF_SIZE + 1];
	char	*s;
	size_t	ret;

	(void)cmd;
	if ((list = listening_socket(sock)) == ERROR)
	{
		ft_putstr("Something went wrong\n");
		return (ERROR);
	}
	write_params(sock, *cmd);
	get_next_line(sock, &s);
	printf("%s\n", s);
	acc = accept_conn(list);
	while ((ret = read(acc, buf, BUF_SIZE)) > 0)
	{
		buf[ret] = 0;
		ft_putstr(buf);
	}
	close(acc);
	ft_strdel(&s);
	return (SUCCESS);
}
