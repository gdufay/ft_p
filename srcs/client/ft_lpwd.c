/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lpwd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 15:35:20 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 10:11:06 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

int		ft_lpwd(int sock, char **params)
{
	char	pwd[PATH_MAX + 1];

	(void)sock;
	(void)params;
	if (getcwd(pwd, PATH_MAX) == NULL)
		printf("Problems getting pwd\n");
	else
		printf("Local directory: %s\n", pwd);
	return (ERROR);
}
