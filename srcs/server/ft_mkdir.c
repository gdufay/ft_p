/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mkdir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 17:33:00 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/26 11:24:47 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		ft_mkdir(char **params, t_conn *conn)
{
	char	canon[PATH_MAX + PADDING];
	char	host[2 * PATH_MAX];

	if (!*params || ft_strlen(*params) >= PATH_MAX)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	ft_bzero(canon, sizeof(canon));
	if (canonise_path(*params, canon) == NULL)
	{
		replies(*conn, LOCAL_ERROR);
		return ;
	}
	servertohost(canon, host);
	if (mkdir(host, 0755) == -1)
		replies(*conn, FILE_UNVL);
	else
		replies(*conn, DIR_CREATED);
}
