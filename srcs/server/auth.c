/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   auth.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 15:48:12 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/16 11:17:59 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	open_file(void)
{
	char	path[2 * PATH_MAX];

	ft_strcpy(path, g_root.root);
	ft_strcat(path, "/");
	ft_strncat(path, PW_FILE, PATH_MAX - 2);
	return (open(path, O_RDONLY));
}

char		*getentry_pwfile(char *name)
{
	int		fd;
	size_t	len;
	char	*s;

	if (name == NULL || (fd = open_file()) < 0)
		return (NULL);
	len = ft_strlen(name);
	s = NULL;
	while (get_next_line(fd, &s) > 0)
	{
		if (!ft_strncmp(s, name, len))
		{
			close(fd);
			return (s);
		}
		ft_strdel(&s);
	}
	close(fd);
	if (ft_strncmp(s, name, len))
		ft_strdel(&s);
	return (s);
}
