/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_quit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/25 10:29:44 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/16 14:46:56 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		ft_quit(char **params, t_conn *conn)
{
	if (*params)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	replies(*conn, SUCCESS);
	close(conn->ftp.sock);
}
