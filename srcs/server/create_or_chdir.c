/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_or_chdir.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 10:37:39 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/16 11:20:39 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	goto_root(void)
{
	return (chdir(g_root.root));
}

int			create_or_chdir(char *name)
{
	return (goto_root() || (chdir(name) < 0
				&& (mkdir(name, 0755) < 0 || chdir(name) < 0)));
}
