/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_port.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 14:13:52 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/16 14:47:12 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	set_host_and_port(t_conn *conn, char *host, int port)
{
	conn->data.sin.sin_addr.s_addr = inet_addr(host);
	conn->data.sin.sin_port = htons(port);
}

static int	get_host(char host[16], char **split)
{
	char	i;

	i = -1;
	while (++i < 4)
	{
		if (ft_strlen(*(split + i)) > PORT_HOSTLEN)
			return (-1);
		ft_strncat(host, *(split + i), PORT_HOSTLEN);
		if (i < 3)
			ft_strcat(host, ".");
	}
	return (0);
}

static int	get_port(char **split)
{
	unsigned char	val1;
	unsigned char	val2;
	int				ret;

	val1 = ft_atoi(*split);
	val2 = ft_atoi(*(split + 1));
	if (val1 < 0 || val2 < 0)
		return (-1);
	ret = ((val1 << 8) | val2);
	return (ret);
}

void		ft_port(char **params, t_conn *conn)
{
	char	host[16];
	int		port;
	char	**split;

	if ((split = ft_strsplit(*params, ',')) == NULL
			|| ft_tablen(split) != PORT_ARGLEN)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	host[0] = 0;
	if (get_host(host, split) < 0 || (port = get_port(split + 4)) < 0)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	set_host_and_port(conn, host, port);
	replies(*conn, SUCCESS);
	ft_free_tab(&split);
}
