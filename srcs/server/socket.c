/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socket.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 10:52:52 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/11 10:53:18 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int		create_socket(struct sockaddr_in sin,
		int (*ft)(int, struct sockaddr_in*))
{
	int					sock;
	struct protoent		*proto;

	if ((proto = getprotobyname("tcp")) == NULL)
		return (-1);
	sock = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	if (ft(sock, &sin) == -1)
	{
		close(sock);
		return (-1);
	}
	return (sock);
}

int		connect_socket(int sock, struct sockaddr_in *sin)
{
	if (connect(sock, (const struct sockaddr*)sin, sizeof(*sin)) == -1)
	{
		ft_putstr_fd("Error connecting\n", 2);
		return (-1);
	}
	return (0);
}

int		bind_socket(int sock, struct sockaddr_in *sin)
{
	if (bind(sock, (const struct sockaddr*)sin, sizeof(*sin)) == -1)
	{
		ft_putstr_fd("Error binding\n", 2);
		return (-1);
	}
	listen(sock, 42);
	return (0);
}
