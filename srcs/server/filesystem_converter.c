/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filesystem_converter.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 11:03:21 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/26 11:18:32 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

char		*hosttoserver(char *buf)
{
	char	path[PATH_MAX];
	size_t	len;

	if (getcwd(path, PATH_MAX) == NULL)
		return (NULL);
	len = ft_strlen(g_root.root) + g_root.len;
	if (*(path + len) == '/')
		ft_strcat(buf, path + len);
	else
		ft_strcat(buf, "/");
	return (buf);
}

char		*servertohost(char *spath, char *buf)
{
	char	path[PATH_MAX + 2];
	size_t	len;

	ft_strncpy(path, g_root.root, PATH_MAX);
	len = ft_strlen(path);
	if (path[len - 1] != '/')
	{
		path[len] = '/';
		path[++len] = 0;
	}
	ft_strncat(path, g_root.name, PATH_MAX - len);
	len = ft_strlen(path);
	if (path[len - 1] != '/' && *spath != '/')
	{
		path[len] = '/';
		path[++len] = 0;
	}
	ft_strcpy(buf, path);
	ft_strcat(buf, spath);
	return (buf);
}
