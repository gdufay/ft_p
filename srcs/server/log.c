/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   log.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/25 09:34:04 by gdufay            #+#    #+#             */
/*   Updated: 2019/07/25 10:26:49 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		ft_log(int user, char *cmd, char *state)
{
	time_t	t;
	char	*ct;

	t = time(NULL);
	ct = ctime(&t);
	ct[24] = 0;
	printf("%s: user %d: %s %s", ct, user, cmd, state);
}
