/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_retr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 10:45:03 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 11:56:23 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	send_file(int sock, int fd)
{
	ssize_t	ret;
	ssize_t	wr;
	char	buf[BUF_SIZE + 1];

	while ((ret = read(fd, buf, BUF_SIZE)) > 0)
	{
		buf[ret] = 0;
		wr = write(sock, buf, ret);
		if (wr == -1 || wr != ret)
			return (LOCAL_ERROR);
	}
	if (ret == -1)
		return (LOCAL_ERROR);
	return (CONN_CLOSED);
}

static char	*protect_param(char *s, char *ret)
{
	char	canon[PATH_MAX + PADDING];
	size_t	end;

	ft_bzero(canon, sizeof(canon));
	if (canonise_path(s, canon) == NULL)
		return (NULL);
	end = ft_strlen(canon);
	if (*(canon + end - 1) == '/')
		*(canon + end - 1) = 0;
	ret = servertohost(canon, ret);
	return (ret);
}

void		ft_retr(char **params, t_conn *conn)
{
	int		fd;
	int		sock;
	char	protect[2 * PATH_MAX];
	int		rep;

	ft_bzero(protect, sizeof(protect));
	if (!*params || protect_param(*params, protect) == NULL)
		replies(*conn, SYNTAX_ERROR);
	else if ((fd = open(protect, O_RDONLY)) == -1)
		replies(*conn, FILE_UNVL);
	else if ((sock = create_socket(conn->data.sin, connect_socket)) == -1)
	{
		close(fd);
		replies(*conn, NO_DATA_CONN);
	}
	else
	{
		replies(*conn, OPEN_CONN);
		signal(SIGPIPE, SIG_IGN);
		rep = send_file(sock, fd);
		signal(SIGPIPE, SIG_DFL);
		close(fd);
		close(sock);
		replies(*conn, rep);
	}
}
