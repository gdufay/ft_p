/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_req.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 16:54:34 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/16 14:29:30 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void		handle_req(t_conn *conn)
{
	size_t			i;
	static t_cmd	cmds[] = {
		{LIST, ft_ls}, {CWD, ft_cd}, {PWD, ft_pwd}, {MKD, ft_mkdir},
		{QUIT, ft_quit}, {PORT, ft_port}, {RETR, ft_retr},
		{STOR, ft_stor}, {USER, ft_user}, {NLST, ft_ls}, {PASS, ft_pass},
	};

	i = -1;
	while (++i < sizeof(cmds) / sizeof(*cmds))
		if (conn->cmd && ft_strcmp(conn->cmd, cmds[i].cmd) == 0)
		{
			cmds[i].ft(conn->params, conn);
			return ;
		}
	replies(*conn, SYNTAX_ERROR);
}

void			parse_req(t_conn conn)
{
	char			buf[REQ_SIZE];
	char			**split;
	ssize_t			r;

	while ((r = read(conn.ftp.sock, buf, REQ_SIZE - 1)) > 0)
	{
		buf[r] = 0;
		if ((split = ft_strmsplit(buf, " \n\t\r\f\v")) == NULL)
			return ;
		ft_strlower(*split);
		conn.cmd = *split;
		conn.params = split + 1;
		handle_req(&conn);
		ft_free_tab(&split);
	}
}
