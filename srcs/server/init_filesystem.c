/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_filesystem.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 16:24:07 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/11 10:44:15 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			init_g_root(void)
{
	if (getcwd(g_root.root, PATH_MAX) == NULL)
		return (-1);
	if ((g_root.name = ft_strdup(ROOT_DIR)) == NULL)
		return (-1);
	g_root.len = ROOT_DIR_LEN;
	return (0);
}

int			init_filesystem(void)
{
	DIR		*root;

	if ((root = opendir(ROOT_DIR)) == NULL)
		mkdir(ROOT_DIR, 0755);
	else
		closedir(root);
	return (chdir(ROOT_DIR));
}
