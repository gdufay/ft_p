/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pass.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 15:39:18 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 14:21:11 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	ret_ft_pass(t_conn *conn, enum e_replies code, char *s)
{
	conn->auth->auth = NOT_AUTH;
	ft_strdel(&(conn->auth->name));
	if (s)
		free(s);
	replies(*conn, code);
}

/*
** TODO: for more security, put SALT in env
*/

void		ft_pass(char **params, t_conn *conn)
{
	char	*pw;
	char	*s;

	if (conn->auth->auth != IN_PROG)
		return (ret_ft_pass(conn, BAD_CMD_SEQ, NULL));
	if (!*params)
		return (ret_ft_pass(conn, USER_NOT_LOGGED, NULL));
	if ((s = getentry_pwfile(conn->auth->name)) == NULL)
		return (ret_ft_pass(conn, SUPERFLUOUS_CMD, NULL));
	if ((pw = ft_strchr(s, ':')) == NULL)
		return (ret_ft_pass(conn, LOCAL_ERROR, s));
	if (ft_strcmp(pw + 1, crypt(*params, SALT))
			|| create_or_chdir(conn->auth->name) < 0)
		return (ret_ft_pass(conn, USER_NOT_LOGGED, s));
	ft_strdel(&g_root.name);
	g_root.len = 0;
	g_root.name = ft_strdup(conn->auth->name);
	g_root.len = ft_strlen(g_root.name) + 1;
	ret_ft_pass(conn, USER_LOGGED, s);
	conn->auth->auth = AUTH;
}
