/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mv_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 13:12:50 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 10:49:26 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	remove_parent(char *np, int len)
{
	np[len - 1] = 0;
	while (--len && np[len] != '/')
		np[len] = 0;
	return (++len);
}

static char	*path_conversion(char *np, char *curpath, int len)
{
	char	*next;
	int		diff;

	if (!*curpath)
		return (np);
	next = ft_strchr(curpath, '/');
	diff = next - curpath + 1;
	if (!len)
	{
		*np = '/';
		len++;
	}
	else if (ft_strncmp(curpath, "../", 3) && ft_strncmp(curpath, "./", 2)
			&& (*curpath != '/'))
	{
		ft_strncpy(np + len, curpath, diff);
		len += diff;
	}
	else if (!ft_strncmp(curpath, "../", 3) && ft_strcmp(np, "/"))
		len = remove_parent(np, len);
	np[len] = 0;
	return (path_conversion(np, next + 1, len));
}

static char	*get_curpath(char *path, char *buf)
{
	size_t	len;

	if (*path == '/')
		ft_strcpy(buf, path);
	else
	{
		hosttoserver(buf);
		len = ft_strlen(buf);
		if (buf[len - 1] != '/')
		{
			buf[len] = '/';
			buf[++len] = 0;
		}
		ft_strcat(buf + len, path);
	}
	len = ft_strlen(buf);
	if (buf[len - 1] != '/')
	{
		buf[len] = '/';
		buf[len + 1] = 0;
	}
	return (buf);
}

char		*canonise_path(char *path, char *buf)
{
	char	curpath[2 * PATH_MAX];

	ft_bzero(curpath, sizeof(curpath));
	get_curpath(path, curpath);
	if (ft_strlen(curpath) >= PATH_MAX)
		return (NULL);
	return (path_conversion(buf, curpath, 0));
}
