/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 17:32:50 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/26 10:55:09 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		ft_cd(char **params, t_conn *conn)
{
	char	buf[PATH_MAX + PADDING];
	char	np[2 * PATH_MAX];

	if (*params == NULL || *(params + 1) || ft_strlen(*params) >= PATH_MAX)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	ft_bzero(buf, sizeof(buf));
	if (canonise_path(*params, buf) == NULL)
	{
		replies(*conn, LOCAL_ERROR);
		return ;
	}
	servertohost(buf, np);
	if (chdir(np) == -1)
		replies(*conn, FILE_UNVL);
	else
		replies(*conn, SUCCESS);
}
