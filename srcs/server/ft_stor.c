/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stor.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 10:56:52 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/18 10:54:44 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	recv_file(int fd, t_conn *conn)
{
	int		sock;
	char	buf[REQ_SIZE + 1];
	ssize_t	ret;

	if ((sock = create_socket(conn->data.sin, connect_socket)) == -1)
	{
		replies(*conn, NO_DATA_CONN);
		return ;
	}
	replies(*conn, OPEN_CONN);
	while ((ret = read(sock, buf, REQ_SIZE)) > 0)
	{
		buf[ret] = 0;
		if (ret != write(fd, buf, ret))
		{
			close(sock);
			replies(*conn, LOCAL_ERROR);
			return ;
		}
	}
	replies(*conn, CONN_CLOSED);
	close(sock);
}

void		ft_stor(char **params, t_conn *conn)
{
	int		fd;
	char	*protect;

	if (!*params)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	if ((protect = ft_strrchr(*params, '/')) == NULL)
		protect = *params;
	else
		protect += 1;
	if ((fd = open(protect, O_RDWR | O_CREAT | O_TRUNC, 0644)) == -1)
	{
		replies(*conn, FILE_UNVL);
		return ;
	}
	recv_file(fd, conn);
	close(fd);
}
