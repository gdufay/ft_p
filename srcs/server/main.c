/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 14:15:09 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/05 16:45:24 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

t_root		g_root;

static void	usage(char *name)
{
	ft_putstr_fd(name, STDERR_FILENO);
	ft_putstr_fd(" port\n", STDERR_FILENO);
	exit(EXIT_FAILURE);
}

int			main(int ac, char **av)
{
	int					port;
	int					sock;
	struct sockaddr_in	sin;

	if (ac != 2)
		usage(av[0]);
	port = ft_atoi(av[1]);
	if (port < 0 || port > PORT_MAXLEN)
		exit(EXIT_FAILURE);
	if (init_g_root() == -1 || init_filesystem() == -1)
	{
		ft_putstr_fd("Error init filesystem\n", STDERR_FILENO);
		return (1);
	}
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	if ((sock = create_socket(sin, bind_socket)) == -1)
		return (1);
	server(sock);
	close(sock);
	ft_strdel(&g_root.name);
	return (0);
}
