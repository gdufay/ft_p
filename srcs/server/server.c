/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:20:55 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/19 11:47:19 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <server.h>

static struct s_conn	set_s_conn(int user, char *cmd, char **params,
		struct sockaddr_in csin)
{
	struct s_conn	conn;
	struct s_auth	auth;

	conn.user = user;
	conn.name = NULL;
	conn.cmd = cmd;
	conn.params = params;
	conn.ftp = (struct s_sock){-1, csin};
	conn.data = (struct s_sock){-1, csin};
	auth.auth = NOT_AUTH;
	auth.name = NULL;
	conn.auth = &auth;
	return (conn);
}

static int				ft_fork(int cs, int counter, struct sockaddr_in csin)
{
	int				pid;
	int				stat;
	struct s_conn	conn;

	if ((pid = fork()) == -1)
		close(cs);
	else if (pid > 0)
	{
		wait4(pid, &stat, WNOHANG, 0);
		close(cs);
		counter++;
	}
	else if (pid == 0)
	{
		counter++;
		conn = set_s_conn(counter, "connect", NULL, csin);
		conn.ftp.sock = cs;
		replies(conn, USER_CONNECT);
		parse_req(conn);
		close(cs);
		exit(EXIT_SUCCESS);
	}
	return (counter);
}

void					server(int sock)
{
	int					cs;
	unsigned int		cslen;
	struct sockaddr_in	csin;
	int					counter;

	counter = 0;
	cslen = sizeof(csin);
	while (1)
	{
		signal(SIGCHLD, SIG_IGN);
		cs = accept(sock, (struct sockaddr*)&csin, &cslen);
		if ((counter = ft_fork(cs, counter, csin)) > 1000)
		{
			printf("Counter reinitiliased\n");
			counter = 0;
		}
	}
}
