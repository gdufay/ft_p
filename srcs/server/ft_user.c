/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_user.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 14:17:03 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 13:41:28 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	test_pw(char *name, t_conn *conn)
{
	char	*s;

	s = getentry_pwfile(name);
	if (s)
	{
		conn->auth->auth = IN_PROG;
		ft_strdel(&(conn->auth->name));
		free(s);
		conn->auth->name = ft_strdup(name);
		replies(*conn, NEED_PW);
		return (1);
	}
	else
		return (0);
}

void		ft_user(char **params, t_conn *conn)
{
	if (!*params || ft_strchr(*params, '/'))
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	if (test_pw(*params, conn))
		return ;
	ft_strdel(&g_root.name);
	g_root.len = 0;
	if (create_or_chdir(*params))
	{
		replies(*conn, CLOSE_CTRL_CONN);
		close(conn->ftp.sock);
		return ;
	}
	g_root.name = ft_strdup(*params);
	g_root.len = ft_strlen(g_root.name) + 1;
	replies(*conn, USER_LOGGED);
}
