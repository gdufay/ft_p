/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pwd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 17:32:39 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/26 11:19:37 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		ft_pwd(char **params, t_conn *conn)
{
	char	buf[PATH_MAX + 2];

	if (*params)
	{
		replies(*conn, SYNTAX_ERROR);
		return ;
	}
	buf[0] = '"';
	buf[1] = 0;
	if (hosttoserver(buf) == NULL)
		replies(*conn, LOCAL_ERROR);
	else
	{
		ft_strcat(buf, "\"");
		custom_replies(*conn, PWD_CMD, buf);
	}
}
