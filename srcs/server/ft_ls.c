/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 17:24:17 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 14:12:12 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	child_process(t_conn conn, char *params)
{
	int		sock;

	if ((sock = create_socket(conn.data.sin, connect_socket)) == -1)
		exit(LS_ERR);
	replies(conn, OPEN_CONN);
	if (dup2(sock, STDOUT_FILENO) < 0)
	{
		close(sock);
		exit(LS_ERR);
	}
	close(sock);
	if (!ft_strcmp(conn.cmd, LIST))
		execl(LS_PATH, LS_PATH, LS_OPT, params, NULL);
	else
		execl(LS_PATH, LS_PATH, NULL);
	exit(EXIT_SUCCESS);
}

static int	protect_params(char *param, char *host)
{
	char	canon[PATH_MAX + PADDING];
	size_t	len;

	ft_bzero(canon, sizeof(canon));
	if (canonise_path(param, canon) == NULL)
		return (-1);
	servertohost(canon, host);
	len = ft_strlen(host);
	if (len && host[--len] == '/')
		host[len] = 0;
	return (0);
}

void		ft_ls(char **params, t_conn *conn)
{
	int		pid;
	int		stat;
	char	host[2 * PATH_MAX];

	if (*params && protect_params(*params, host) == -1)
	{
		replies(*conn, LOCAL_ERROR);
		return ;
	}
	if ((pid = fork()) == -1)
	{
		replies(*conn, LOCAL_ERROR);
		return ;
	}
	else if (pid > 0)
		wait4(pid, &stat, 0, 0);
	else
		child_process(*conn, *params ? host : NULL);
	if (WEXITSTATUS(stat) == LS_ERR)
		replies(*conn, NO_DATA_CONN);
	else
		replies(*conn, CONN_CLOSED);
}
