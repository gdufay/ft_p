/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replies.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <gdufay@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 13:40:52 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/27 13:40:56 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

const char *const g_replies_msg[] = {
	[OPEN_CONN] = "150 File status okay; about to open data connection.\n",
	[SUCCESS] = "200 Command okay.\n",
	[SUPERFLUOUS_CMD] = "202 Command not implemented, superfluous.\n",
	[USER_CONNECT] = "220 Service ready for new user.\n",
	[CONN_CLOSED] = "226 Closing data connection. Requested action done.\n",
	[USER_LOGGED] = "230 User logged in, proceed.\n",
	[DIR_CREATED] = "257 \"PATHNAME\" created.\n",
	[NEED_PW] = "331 User name okay, need password.\n",
	[CLOSE_CTRL_CONN] = "421 Service unavailable, closing connection.\n",
	[NO_DATA_CONN] = "425 Can't open data connection.\n",
	[LOCAL_ERROR] = "451 Requested action aborted: local error.\n",
	[SYNTAX_ERROR] = "500 Syntax error, command unrecognized.\n",
	[CMD_NOT_IMPLM] = "502 Command not implemented.\n",
	[BAD_CMD_SEQ] = "503 Bad sequence of commands.\n",
	[USER_NOT_LOGGED] = "530 Not logged in.\n",
	[FILE_UNVL] = "550 Requested action not taken. File unavailable.\n",
	[PWD_CMD] = "257 ",
};

void		custom_replies(t_conn conn, enum e_replies reply, char *s)
{
	char	buf[REQ_SIZE];

	ft_strcpy(buf, g_replies_msg[reply]);
	ft_strncat(buf, s, REQ_SIZE - ft_strlen(g_replies_msg[reply] - 2));
	ft_strcat(buf, "\n");
	ft_log(conn.user, conn.cmd, buf);
	write(conn.ftp.sock, buf, ft_strlen(buf));
}

void		replies(t_conn conn, enum e_replies reply)
{
	ft_log(conn.user, conn.cmd, (char*)g_replies_msg[reply]);
	write(conn.ftp.sock, g_replies_msg[reply], ft_strlen(g_replies_msg[reply]));
}

/*
** No use of multi_line_replies
**
**
**static char	*join(char *s, char *content)
**{
**	char	*tmp;
**
**	if (!s)
**		return (s);
**	tmp = ft_strjoin(s, content);
**	ft_strdel(&s);
**	return (tmp);
**}
**
**void		multi_line_replies(t_conn conn, char *code[4], t_list *lst)
**{
**	char	buf[16];
**	char	*s;
**
**	ft_strcpy(buf, code[0]);
**	ft_strcat(buf, "-");
**	ft_strcat(buf, code[1]);
**	ft_strcat(buf, H_CRLF);
**	ft_log(conn.user, conn.cmd, buf);
**	s = ft_strdup(buf);
**	while (lst)
**	{
**		s = join(s, lst->content);
**		lst = lst->next;
**	}
**	ft_strcpy(buf, code[2]);
**	ft_strcat(buf, " ");
**	ft_strcat(buf, code[3]);
**	ft_strcat(buf, H_CRLF);
**	s = join(s, buf);
**	write(conn.ftp.sock, s, ft_strlen(s));
**	ft_strdel(&s);
**}
*/
