CLIENT = client
SERVER = server

CC = gcc
CFLAGS = -Wall -Wextra -Werror -g 

ICLD = includes/
_INCLUDES = client.h server.h ft_p.h
INCLUDES = $(addprefix $(ICLD), $(_INCLUDES))
LIB_ICLD = libft/includes/

SRCD = srcs/
CLIENT_SRCD = $(SRCD)client/
SERVER_SRCD = $(SRCD)server/

_CLIENT_SRCS = client.c prompt.c handle_replies.c		\
			   command.c ft_ls.c ft_cd.c ft_quit.c		\
			   ft_pwd.c ft_get.c ft_put.c ft_port.c		\
			   ft_user.c ft_mkdir.c data_conn.c			\
			   ft_lls.c ft_lcd.c ft_lpwd.c
CLIENT_SRCS = $(addprefix $(CLIENT_SRCD), $(_CLIENT_SRCS))
CLIENT_OBJS = $(CLIENT_SRCS:.c=.o)

_SERVER_SRCS = server.c init_filesystem.c parse_req.c	\
			   ft_ls.c ft_cd.c ft_pwd.c ft_mkdir.c 		\
			   replies.c mv_file.c log.c ft_quit.c		\
			   main.c socket.c filesystem_converter.c	\
			   ft_port.c ft_retr.c ft_stor.c ft_user.c	\
			   auth.c ft_pass.c create_or_chdir.c
			   
SERVER_SRCS = $(addprefix $(SERVER_SRCD), $(_SERVER_SRCS))
SERVER_OBJS = $(SERVER_SRCS:.c=.o)

LIBRARIES = libft/libft.a

# COLORS
CRED=\033[91m
CGREEN=\033[38;2;0;255;145m
CEND=\033[0m

.PHONY: all
all: $(CLIENT) $(SERVER)

.PHONY: libft
libft:
	@make -j -s -C libft

$(CLIENT): $(CLIENT_OBJS)
	@printf "\r\033[K$(CGREEN)Creating client$(CEND): $(CLIENT)\n"
	@$(CC) $(CFLAGS) -o $@ $^ $(LIBRARIES)
	@echo  "$(CLIENT): $(CGREEN)done$(CEND)"

$(SERVER): $(SERVER_OBJS)
	@printf "\r\033[K$(CGREEN)Creating server$(CEND): $(SERVER)\n"
	@$(CC) $(CFLAGS) -o $@ $^ $(LIBRARIES)
	@echo  "$(SERVER): $(CGREEN)done$(CEND)"

%.o: %.c $(INCLUDES) libft
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD) -I$(LIB_ICLD)

.PHONY: clean
clean:
	@echo "$(CRED)Cleaning$(CEND): $(CLIENT) $(SERVER)"
	@rm -f $(SERVER_OBJS) $(CLIENT_OBJS)
	@make -s -C libft clean

.PHONY: fclean
fclean: clean
	@echo "$(CRED)Full cleaning$(CEND): $(CLIENT) $(SERVER)"
	@rm -f $(CLIENT) $(SERVER)
	@make -s -C libft fclean

.PHONY: re
re: fclean all
